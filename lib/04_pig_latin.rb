def translate(phrase)
  result = ""
  vowel = "aeiou"
  phrase_split = phrase.split(" ")
  phrase_split.map do |word|
    word_char = word.split("")
    while vowel.include?(word_char[0]) == false
      if word_char[0] == "q"
        word_char = word_char.drop(2) + word_char.take(2)
      else
        word_char = word_char.drop(1) + word_char.take(1)
      end
    end
    result += (word_char.join + "ay ")
  end
  result = result[0...result.length - 1]
end
