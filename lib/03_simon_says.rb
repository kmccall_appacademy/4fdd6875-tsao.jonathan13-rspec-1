def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, n=1)
  result = str + " " + str
  (n - 2).times {result += " " + str}
  result
end

def start_of_word(word, n)
  result = ""
  arr = word.split("")
  for i in 0...n
    result += arr[i]
  end
  result
end

def first_word(phrase)
  phrase.split(" ").first
end

def titleize(sent)
  sent_split = sent.split(" ")
  result = sent_split[0].capitalize
  return result if sent_split.length == 1
  for i in 1...sent_split.length
    if i == (sent_split.length - 1)
      result += " " + sent_split[i].capitalize
    elsif sent_split[i].length > 4
      result += " " + sent_split[i].capitalize
    else
      result += " " + sent_split[i]
    end
  end
  result
end
